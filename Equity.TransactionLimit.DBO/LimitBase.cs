﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Equity.TransactionLimit.DBO
{
    public class LimitBase
    {
        public int Id { get; set; }
        public string Channel { get; set; }
        public string CountryCode { get; set; }
        public string Currency { get; set; }
        public string BankCode { get; set; }
        public string SchemeCode { get; set; }
        public string ProductType { get; set; }
        public string DailyLimit { get; set; }
        public string WeeklyLimit { get; set; }
        public string MonthlyLimit { get; set; }
        public string YearlyLimit { get; set; }
        public string TransactionLimit { get; set; }
    }
}
