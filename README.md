# Juann Strauss

## Notes
- Certain HTTP response codes will be thrown before the Controller action receives focus, therefore the body returned by the Response is not the JSON object outlined in the specification document. 
- Each Controller Action is specifically annotated to only allow a content-type of application/json.

### Assumptions
- Global Transaction Limit is "global" to the entire bank
- Transaction Limit is applied to a bank account, and not to a client. This means a client will have separate limits per account if they have more than one, e.g. savings, investment, credit card, bond.
- We are querying only Equity Bank's accounts and therefore we don't search on Country Code, Currency, etc. AccountId by itself acts as a unique ID for our Where clause
- Third party Bearer Token service will be used, as opposed to the API being the token authority.

### Data Access
- Data Access code was mainly done as an afterthought. The in-memory DB is just a convenient self-contained way of storing our data. The code was written with this in mind. Code written for Entity Framework or some other concrete data storage implementation would look slightly different, e.g. there will be no need to generate a unique identifier on the fly or to create a separate Add(object) for each collection.

### JWT Authentication and Authorization
- JWT auth was written without an explicit authority or audience, however there is commented-out code demonstrating how to easily use a 3rd party provider, in this instance, Azure AD B2C.
- It is assumed that demonstrating familiarity with the concept of Token-based auth is sufficient for this assessment.

### Validation
- The request validation code is arbitrary although it fulfills the requirements as set out and inferred from the 3 provided PDF documents. I chose not to use data annotations to express the validation rules to show that there are alternative methods which provides more fine-grained control over input validation. Data Annotation is however something I am very much familiar with.

### Controller
- I have added the 415 Status code as a possible return type because if the Content-Type header is missing or is not of type application/json, a 415 "Unsupported Media Type" is automatically thrown.

### API documentation
- The API is documented using Swashbuckle and Swagger. When running the application in a browser, the user is automatically shown the Swagger API documentation page.
- Alternatively, the documentation can be viewed at the following endpoint in a browser: /swagger/index.html

### Application settings
- Application settings are done in appsettings.json, however I am more inclined towards storing the settings in environment variables when the application is built for Docker and Kubernetes. This makes it more easily configurable in production.

### Testing the app
- Hitting F5 in Visual Studio or executing 'dotnet run' in VS Code is sufficient.
- If you want to test the API while side-stepping the Bearer Token auth, simply comment out the [Authorise] attribute on the LimitController. The Controller will then not authenticate the request and will simply execute the code as normal.