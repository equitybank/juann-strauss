﻿using Equity.TransactionLimit.DBO;
using Equity.TransactionLimit.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Equity.TransactionLimit.DataAccess.InMemory
{
    public class InMemoryDb : IDataAccess
    {
        private static List<DBO.GlobalLimit> GlobalLimits { get; set; }
        private static List<DBO.TransactionLimit> TransactionLimits { get; set; }

        public InMemoryDb()
        {
            if (GlobalLimits == null)
            {
                GlobalLimits = new List<GlobalLimit>();
            }

            if (TransactionLimits == null)
            {
                TransactionLimits = new List<DBO.TransactionLimit>();
            }
        }

        public void AddGlobalLimit(GlobalLimit globalLimit)
        {
            GlobalLimits.Add(globalLimit);
        }

        public void AddTransactionLimit(DBO.TransactionLimit transactionLimit)
        {
            TransactionLimits.Add(transactionLimit);
        }

        public IQueryable<DBO.GlobalLimit> GetGlobalLimits()
        {
            return GlobalLimits.AsQueryable();
        }

        public IQueryable<DBO.TransactionLimit> GetTransactionLimits()
        {
            return TransactionLimits.AsQueryable();
        }
    }
}
