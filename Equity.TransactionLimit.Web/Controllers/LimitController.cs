﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Equity.TransactionLimit.DTO.Models.Requests;
using Equity.TransactionLimit.DTO.Models.Responses;
using Equity.TransactionLimit.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.Swagger.Annotations;

namespace Equity.TransactionLimit.Web.Controllers
{
    [Route("v1")]
    [Authorize]
    [ApiController]
    public class LimitController : ControllerBase
    {
        private ILimitService LimitService { get; }
        public LimitController(ILimitService limitService)
        {
            this.LimitService = limitService;
        }
                

        [Route("globallimit")]
        [Microsoft.AspNetCore.Mvc.HttpPost()]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(200, Type = typeof(LimitResponse))] //OK
        [ProducesResponseType(400, Type = typeof(LimitResponse))]//Bad Request
        [ProducesResponseType(401, Type = typeof(LimitResponse))]//Unauthorized
        [ProducesResponseType(403, Type = typeof(LimitResponse))]//Forbidden
        [ProducesResponseType(404, Type = typeof(LimitResponse))]//Not Found
        [ProducesResponseType(415)]//Unsupported Media Type
        [ProducesResponseType(500, Type = typeof(LimitResponse))]//Internal Server Error
        public async Task<IActionResult> CreateGlobalLimitAsync(CreateGlobalLimitRequestParameter globalLimitRequest)
        {
            LimitResponse limitresponse = await LimitService.CreateGlobalLimitAsync(globalLimitRequest);
            return StatusCode((int)limitresponse.HttpStatusCode, limitresponse);
            
        }

        [Route("transactionlimit")]
        [Microsoft.AspNetCore.Mvc.HttpPost()]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(200, Type = typeof(LimitResponse))] //OK
        [ProducesResponseType(400, Type = typeof(LimitResponse))]//Bad Request
        [ProducesResponseType(401, Type = typeof(LimitResponse))]//Unauthorized
        [ProducesResponseType(403, Type = typeof(LimitResponse))]//Forbidden
        [ProducesResponseType(404, Type = typeof(LimitResponse))]//Not Found
        [ProducesResponseType(415)]//Unsupported Media Type
        [ProducesResponseType(500, Type = typeof(LimitResponse))]//Internal Server Error
        public async Task<IActionResult> CreateTransactionLimitAsync(CreateTransactionLimitRequestParameter transactionLimitRequest)
        {
            LimitResponse limitresponse = await LimitService.CreateTransactionLimitAsync(transactionLimitRequest);

            return StatusCode((int)limitresponse.HttpStatusCode, limitresponse);
        }

        [Route("getlimit")]
        [Microsoft.AspNetCore.Mvc.HttpPost()]
        [Consumes("application/json")]
        [Produces("application/json")]
        [ProducesResponseType(200, Type = typeof(LimitResponse))] //OK
        [ProducesResponseType(400, Type = typeof(LimitResponse))]//Bad Request
        [ProducesResponseType(401, Type = typeof(LimitResponse))]//Unauthorized
        [ProducesResponseType(403, Type = typeof(LimitResponse))]//Forbidden
        [ProducesResponseType(404, Type = typeof(LimitResponse))]//Not Found
        [ProducesResponseType(415)]//Unsupported Media Type
        [ProducesResponseType(500, Type = typeof(LimitResponse))]//Internal Server Error
        public async Task<IActionResult> GetTransactionLimitAsync(GetTransactionLimitRequestParameter transactionLimitRequest)
        {
            LimitResponse limitresponse = await LimitService.GetTransactionLimitAsync(transactionLimitRequest);

            return StatusCode((int)limitresponse.HttpStatusCode, limitresponse);
        }
    }
}