using System.Text;
using Equity.TransactionLimit.BusinessLogic;
using Equity.TransactionLimit.DataAccess.InMemory;
using Equity.TransactionLimit.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace Equity.TransactionLimit.Web
{
    public class Startup
    {
        public static string ScopeRead;
        public static string ScopeWrite;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            

            #region JWT
            var key = Encoding.ASCII.GetBytes(Configuration["JWT:key"]);
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    #region configure 3rd party authority, e.g. Azure AD B2C
                    //jwtOptions.Authority = $"https://{configuration["AzureAdB2C:TenantName"]}.b2clogin.com/tfp/{configuration["AzureAdB2C:Tenant"]}/{configuration["AzureAdB2C:Policy"]}/v2.0/";
                    //jwtOptions.Audience = configuration["AzureAdB2C:ClientId"];
                    #endregion
                    
                    #if DEBUG
                    x.RequireHttpsMetadata = false;
                    #endif

                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
            #endregion

            //For our purposes all services can be Transient (i.e. Request-scoped). 
            //Our InMemoryDb's Lists won't be thread-safe, but it will serve our purpose which is to demonstrate coding ability
            services.AddTransient<IDataAccess, InMemoryDb>();
            services.AddTransient<ILimitService, LimitService>();
            services.AddTransient<IValidationService, ValidationService>();

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Equity Bank Transaction Limit API", Version = "v1", Description = "Please view README.md in repository for comments about this assessment: <a href=\"https://gitlab.com/equitybank/juann-strauss\" target=\"_blank\">https://gitlab.com/equitybank/juann-strauss</a>" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\". To get a token, make a call to the appropriate Authority e.g. Azure AD B2C and then paste the supplied token below.",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            ScopeRead = Configuration["AzureAdB2C:ScopeRead"];
            ScopeWrite = Configuration["AzureAdB2C:ScopeWrite"];
             
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Equity Bank Transaction Limit API");
                c.RoutePrefix = string.Empty;
            });


            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
