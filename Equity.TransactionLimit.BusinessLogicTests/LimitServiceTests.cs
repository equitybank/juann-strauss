﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Equity.TransactionLimit.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Text;
using Equity.TransactionLimit.DataAccess.InMemory;
using System.Threading.Tasks;

namespace Equity.TransactionLimit.BusinessLogic.Tests
{
    [TestClass()]
    //Integration tests
    public class LimitServiceTests
    {
        LimitService GetLimitService()
        {
            var ret = new LimitService(new ValidationService(), new InMemoryDb());
            return ret;
        }
        [TestMethod()]
        public void LimitServiceTest()
        {
            try
            {
                var logic = GetLimitService();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [TestMethod()]
        public async Task CreateGlobalLimitTestSuccess()
        {
            var logic = GetLimitService();
            var model = new DTO.Models.Requests.CreateGlobalLimitRequestParameter() {
                Rid = "1",
                ProductType = "sendToMpesa",
                DailyLimit = "1000000",
                WeeklyLimit = "1000000",
                MonthlyLimit = "1000000",
                YearlyLimit = "1000000",
                TransactionLimit = "3000",
                SchemeCode = "LA324",
                Channel = "Way4",
                CountryCode = "KE",
                Currency = "KES",
                BankCode = "54"
            };
            var res = await logic.CreateGlobalLimitAsync(model);

            Assert.IsTrue(res.status);
        }

        [TestMethod()]
        public async Task CreateGlobalLimitTestFail()
        {
            var logic = GetLimitService();
            var model = new DTO.Models.Requests.CreateGlobalLimitRequestParameter()
            {
                Rid = "1",
                ProductType = "sendToMpesa",
                DailyLimit = "-1000000",//should be invalid
                WeeklyLimit = "1000000",
                MonthlyLimit = "1000000",
                YearlyLimit = "1000000",
                TransactionLimit = "3000",
                SchemeCode = "LA324",
                Channel = "Way4",
                CountryCode = "KE",
                Currency = "KES",
                BankCode = "54"
            };
            var res = await logic.CreateGlobalLimitAsync(model);

            Assert.IsFalse(res.status);
        }

        [TestMethod()]
        public async Task CreateTransactionLimitTestSuccess()
        {
            var logic = GetLimitService();
            var model = new DTO.Models.Requests.CreateTransactionLimitRequestParameter()
            {
                Rid = "1",
                AccountId = "01701934343512",
                CustomerType = "individual",
                ProductType = "fund transfer",
                DailyLimit = "1000000",
                WeeklyLimit = "1000000",
                MonthlyLimit = "1000000",
                YearlyLimit = "7",
                TransactionLimit = "3000",
                SchemeCode = "LA324",
                Channel = "Way4",
                CountryCode = "KE",
                Currency = "KES",
                BankCode = "54"
            };

            var res = await logic.CreateTransactionLimitAsync(model);

            Assert.IsTrue(res.status);
        }

        [TestMethod()]
        public async Task CreateTransactionLimitTestFail()
        {
            var logic = GetLimitService();
            var model = new DTO.Models.Requests.CreateTransactionLimitRequestParameter()
            {
                Rid = "1",
                AccountId = "",//mandatory
                CustomerType = "individual",
                ProductType = "fund transfer",
                DailyLimit = "1000000",
                WeeklyLimit = "1000000",
                MonthlyLimit = "1000000",
                YearlyLimit = "7",
                TransactionLimit = "3000",
                SchemeCode = "LA324",
                Channel = "Way4",
                CountryCode = "KE",
                Currency = "KES",
                BankCode = "54"
            };

            var res = await logic.CreateTransactionLimitAsync(model);

            Assert.IsFalse(res.status);
        }

        [TestMethod()]
        public async Task GetTransactionLimitTestSucceed()
        {
            var logic = GetLimitService();

            //create our sample data
            var sampledata = new DTO.Models.Requests.CreateTransactionLimitRequestParameter()
            {
                Rid = "1",
                AccountId = "01701934343513",
                CustomerType = "individual",
                ProductType = "fund transfer",
                DailyLimit = "1000000",
                WeeklyLimit = "1000000",
                MonthlyLimit = "1000000",
                YearlyLimit = "7",
                TransactionLimit = "3000",
                SchemeCode = "LA324",
                Channel = "Way4",
                CountryCode = "KE",
                Currency = "KES",
                BankCode = "54"
            };
            await logic.CreateTransactionLimitAsync(sampledata);

            var model = new DTO.Models.Requests.GetTransactionLimitRequestParameter()
            {
                Rid = "1",
                AccountId = "01701934343513",
                SchemeCode = "LA324",
                Channel = "Way4",
                CountryCode = "KE",
                Currency = "KES",
                BankCode = "54"
            };

            var res = await logic.GetTransactionLimitAsync(model);

            Assert.IsTrue(res.status);
        }

        [TestMethod()]
        public async Task GetTransactionLimitTestFail()
        {
            var logic = GetLimitService();
            var model = new DTO.Models.Requests.GetTransactionLimitRequestParameter()
            {
                Rid = "1",
                AccountId = "01701934343513",
                SchemeCode = "LA324",
                Channel = "Way4",
                CountryCode = "ZA",//Must be KE
                Currency = "KES",
                BankCode = "54"
            };

            var res = await logic.GetTransactionLimitAsync(model);

            Assert.IsFalse(res.status);
        }
    }
}