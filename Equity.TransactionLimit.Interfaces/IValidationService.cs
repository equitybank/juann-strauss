﻿using Equity.TransactionLimit.DTO.Models.Requests;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Equity.TransactionLimit.Interfaces
{
    public interface IValidationService
    {
        Task<bool> ValidateRequest(CreateGlobalLimitRequestParameter globalLimitRequest);
        Task<bool> ValidateRequest(CreateTransactionLimitRequestParameter transactionLimitRequest);
        Task<bool> ValidateRequest(GetTransactionLimitRequestParameter transactionLimitRequest);

    }
}
