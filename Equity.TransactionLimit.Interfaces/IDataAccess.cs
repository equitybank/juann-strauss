﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Equity.TransactionLimit.Interfaces
{
    //Normally we would return an IQueryable<TEntity> or DBSet<TEntity> but we're simply returning a List for the sake of simplicity
    public interface IDataAccess
    {
        IQueryable<DBO.GlobalLimit> GetGlobalLimits();

        IQueryable<DBO.TransactionLimit> GetTransactionLimits();
        void AddGlobalLimit(DBO.GlobalLimit globalLimit);
        void AddTransactionLimit(DBO.TransactionLimit transactionLimit);
    }
}
