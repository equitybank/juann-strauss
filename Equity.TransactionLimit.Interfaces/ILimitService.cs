﻿using Equity.TransactionLimit.DTO.Models.Requests;
using Equity.TransactionLimit.DTO.Models.Responses;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Equity.TransactionLimit.Interfaces
{
    public interface ILimitService
    {
        Task<LimitResponse> CreateGlobalLimitAsync(CreateGlobalLimitRequestParameter globalLimitRequest);
        Task<LimitResponse> CreateTransactionLimitAsync(CreateTransactionLimitRequestParameter transactionLimitRequest);
        Task<LimitResponse> GetTransactionLimitAsync(GetTransactionLimitRequestParameter transactionLimitRequest);
    }
}
