﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Equity.TransactionLimit.DTO.Models.Requests
{
    public class LimitRequestBase
    {
        public string Rid { get; set; }
        public string Channel { get; set; }
        public string CountryCode { get; set; }
        public string Currency { get; set; }
        public string BankCode { get; set; }
        public string SchemeCode { get; set; }
    }
}
