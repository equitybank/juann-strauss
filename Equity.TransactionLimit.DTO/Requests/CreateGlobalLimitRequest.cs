﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Equity.TransactionLimit.DTO.Models.Requests
{
    public class CreateGlobalLimitRequestParameter : LimitRequestBase
    {
        public string ProductType { get; set; }
        public string DailyLimit { get; set; }
        public string WeeklyLimit { get; set; }
        public string MonthlyLimit { get; set; }
        public string YearlyLimit { get; set; }
        public string TransactionLimit { get; set; }//was spelled tansactionLimit in sample documentation.

    }
}
