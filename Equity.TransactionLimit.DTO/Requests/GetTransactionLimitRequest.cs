﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Equity.TransactionLimit.DTO.Models.Requests
{
    public class GetTransactionLimitRequestParameter : LimitRequestBase
    {
        public string AccountId { get; set; }
        
    }
}
