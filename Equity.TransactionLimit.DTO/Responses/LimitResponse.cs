﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Equity.TransactionLimit.DTO.Models.Responses
{
    public class LimitResponse
    {
        //Example response wasn't well-formed in sample doc. It was obvious that the below was the intended formatting so we went ahead with it
        public string rid { get; set; }
        public bool status { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        [JsonIgnore]
        public HttpStatusCode HttpStatusCode { get; set; }
        public Limit Limit { get; set; }

        public LimitResponse()
        {
            this.Limit = new Limit();
        }
    }
}
