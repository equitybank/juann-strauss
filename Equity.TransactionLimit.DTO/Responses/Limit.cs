﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Equity.TransactionLimit.DTO.Models.Responses
{
    public class Limit
    {
        public string dailyLimit { get; set; }
        public string weeklyLimit { get; set; }
        public string monthlyLimit { get; set; }
        public string yearlyLimit { get; set; }
        public string transactionLimit { get; set; }
    }
}
