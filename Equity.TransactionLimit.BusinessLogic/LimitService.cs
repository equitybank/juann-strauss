﻿using Equity.TransactionLimit.DTO.Models.Requests;
using Equity.TransactionLimit.DTO.Models.Responses;
using Equity.TransactionLimit.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Equity.TransactionLimit.BusinessLogic
{

    public class LimitService : ILimitService
    {
        private IValidationService Validation;
        private IDataAccess DataAccess;
        public LimitService(IValidationService validationService, IDataAccess dataAccess)
        {
            this.Validation = validationService;
            this.DataAccess = dataAccess; 
        }

        public async Task<LimitResponse> CreateGlobalLimitAsync(CreateGlobalLimitRequestParameter globalLimitRequest)
        {
            var result = await Task.Run(async () =>
            {
                var ret = new LimitResponse();
                ret.rid = globalLimitRequest.Rid;
                bool validationresult = await Validation.ValidateRequest(globalLimitRequest);

                if (!validationresult)//invalid
                {
                    ret.code = "1";
                    ret.status = false;
                    ret.description = "Bad Request";
                    ret.HttpStatusCode = System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                    //a real database technology would allow us to call a FirstOrDefaultAsync() or AsQueryableAsync, but this is not available on our current in-memory stand-in
                    var GlobalLimit = DataAccess.GetGlobalLimits()
                                                .FirstOrDefault(x => x.ProductType.ToLowerInvariant() == globalLimitRequest.ProductType.ToLowerInvariant());
                    if (GlobalLimit == null) // add new
                    {
                        GlobalLimit = new DBO.GlobalLimit()
                        {
                            BankCode = globalLimitRequest.BankCode,
                            Channel = globalLimitRequest.Channel,
                            CountryCode = globalLimitRequest.CountryCode,
                            Currency = globalLimitRequest.Currency,
                            DailyLimit = globalLimitRequest.DailyLimit,
                            Id = DataAccess.GetGlobalLimits() // mimic auto-incrementing Identifier
                                                      .Select(x => x.Id)
                                                      .DefaultIfEmpty(0)
                                                      .Max() + 1,
                            MonthlyLimit = globalLimitRequest.MonthlyLimit,
                            ProductType = globalLimitRequest.ProductType,
                            SchemeCode = globalLimitRequest.SchemeCode,
                            TransactionLimit = globalLimitRequest.TransactionLimit,
                            WeeklyLimit = globalLimitRequest.WeeklyLimit,
                            YearlyLimit = globalLimitRequest.YearlyLimit
                        };
                        DataAccess.AddGlobalLimit(GlobalLimit);
                    }
                    else //update
                    {
                        GlobalLimit.BankCode = globalLimitRequest.BankCode;
                        GlobalLimit.Channel = globalLimitRequest.Channel;
                        GlobalLimit.CountryCode = globalLimitRequest.CountryCode;
                        GlobalLimit.Currency = globalLimitRequest.Currency;
                        GlobalLimit.DailyLimit = globalLimitRequest.DailyLimit;
                        GlobalLimit.Id = DataAccess.GetGlobalLimits() // mimic auto-incrementing Identifier
                                                      .Select(x => x.Id)
                                                      .DefaultIfEmpty(0)
                                                      .Max() + 1;
                        GlobalLimit.MonthlyLimit = globalLimitRequest.MonthlyLimit;
                        GlobalLimit.ProductType = globalLimitRequest.ProductType;
                        GlobalLimit.SchemeCode = globalLimitRequest.SchemeCode;
                        GlobalLimit.TransactionLimit = globalLimitRequest.TransactionLimit;
                        GlobalLimit.WeeklyLimit = globalLimitRequest.WeeklyLimit;
                        GlobalLimit.YearlyLimit = globalLimitRequest.YearlyLimit;
                    }
                    ret.code = "0";
                    ret.status = true;
                    ret.description = "Request processed successfully";
                    ret.HttpStatusCode = System.Net.HttpStatusCode.OK;

                    ret.Limit.dailyLimit = GlobalLimit.DailyLimit;
                    ret.Limit.monthlyLimit = GlobalLimit.MonthlyLimit;
                    ret.Limit.weeklyLimit = GlobalLimit.WeeklyLimit;
                    ret.Limit.yearlyLimit = GlobalLimit.YearlyLimit;
                    ret.Limit.transactionLimit = GlobalLimit.TransactionLimit;
                }

                return ret;
            });
            return result;
        }

        public async Task<LimitResponse> CreateTransactionLimitAsync(CreateTransactionLimitRequestParameter transactionLimitRequest)
        {
           var result = await Task.Run(async () =>
           {
               var ret = new LimitResponse();
               ret.rid = transactionLimitRequest.Rid;

               bool validationresult = await Validation.ValidateRequest(transactionLimitRequest);

               if (!validationresult)//invalid
               {
                   ret.code = "1";
                   ret.status = false;
                   ret.description = "Bad Request";
                   ret.HttpStatusCode = System.Net.HttpStatusCode.BadRequest;
                   
               }
               else
               {
                   //a real database technology would allow us to call a FirstOrDefaultAsync() or AsQueryableAsync, but this is not available on our current in-memory stand-in
                   var transactionlimit = DataAccess.GetTransactionLimits()
                                               .FirstOrDefault(x => x.AccountId.ToLowerInvariant() == transactionLimitRequest.AccountId.ToLowerInvariant());
                   if (transactionlimit == null) // add new
                   {
                       transactionlimit = new DBO.TransactionLimit()
                       {
                           BankCode = transactionLimitRequest.BankCode,
                           Channel = transactionLimitRequest.Channel,
                           CountryCode = transactionLimitRequest.CountryCode,
                           Currency = transactionLimitRequest.Currency,
                           DailyLimit = transactionLimitRequest.DailyLimit,
                           Id = DataAccess.GetTransactionLimits() // mimic auto-incrementing Identifier
                                                     .Select(x => x.Id)
                                                     .DefaultIfEmpty(0)
                                                     .Max() + 1,
                           MonthlyLimit = transactionLimitRequest.MonthlyLimit,
                           ProductType = transactionLimitRequest.ProductType,
                           SchemeCode = transactionLimitRequest.SchemeCode,
                           TransactionLimit = transactionLimitRequest.TransactionLimit,
                           WeeklyLimit = transactionLimitRequest.WeeklyLimit,
                           YearlyLimit = transactionLimitRequest.YearlyLimit,
                           AccountId = transactionLimitRequest.AccountId
                       };
                       DataAccess.AddTransactionLimit(transactionlimit);
                   }
                   else //update
                   {
                       transactionlimit.BankCode = transactionLimitRequest.BankCode;
                       transactionlimit.Channel = transactionLimitRequest.Channel;
                       transactionlimit.CountryCode = transactionLimitRequest.CountryCode;
                       transactionlimit.Currency = transactionLimitRequest.Currency;
                       transactionlimit.DailyLimit = transactionLimitRequest.DailyLimit;
                       transactionlimit.Id = DataAccess.GetTransactionLimits() // mimic auto-incrementing Identifier
                                                   .Select(x => x.Id)
                                                   .DefaultIfEmpty(0)
                                                   .Max() + 1;
                       transactionlimit.MonthlyLimit = transactionLimitRequest.MonthlyLimit;
                       transactionlimit.ProductType = transactionLimitRequest.ProductType;
                       transactionlimit.SchemeCode = transactionLimitRequest.SchemeCode;
                       transactionlimit.TransactionLimit = transactionLimitRequest.TransactionLimit;
                       transactionlimit.WeeklyLimit = transactionLimitRequest.WeeklyLimit;
                       transactionlimit.YearlyLimit = transactionLimitRequest.YearlyLimit;
                       transactionlimit.AccountId = transactionLimitRequest.AccountId;
                   }
                   ret.code = "0";
                   ret.status = true;
                   ret.description = "Request processed successfully";
                   ret.HttpStatusCode = System.Net.HttpStatusCode.OK;

                   ret.Limit.dailyLimit = transactionlimit.DailyLimit;
                   ret.Limit.monthlyLimit = transactionlimit.MonthlyLimit;
                   ret.Limit.weeklyLimit = transactionlimit.WeeklyLimit;
                   ret.Limit.yearlyLimit = transactionlimit.YearlyLimit;
                   ret.Limit.transactionLimit = transactionlimit.TransactionLimit;
               }
               return ret;
           });

            return result;
        }

        public async Task<LimitResponse> GetTransactionLimitAsync(GetTransactionLimitRequestParameter transactionLimitRequest)
        {
                var ret = new LimitResponse();
                ret.rid = transactionLimitRequest.Rid;
                bool validationresult = await Validation.ValidateRequest(transactionLimitRequest);

                if (!validationresult)//invalid
                {
                    ret.code = "1";
                    ret.status = false;
                    ret.description = "Bad Request";
                    ret.HttpStatusCode = System.Net.HttpStatusCode.BadRequest;
                }
                else
                {
                //a real database technology would allow us to call a FirstOrDefaultAsync() or AsQueryableAsync, but this is not available on our current in-memory stand-in
                var transactionlimit = DataAccess.GetTransactionLimits()
                                                     .FirstOrDefault(x => x.AccountId.ToLowerInvariant() == transactionLimitRequest.AccountId.ToLowerInvariant());
                    if (transactionlimit == null)
                    {
                        ret.code = "1";
                        ret.status = false;
                        ret.description = "Not found";
                        ret.HttpStatusCode = System.Net.HttpStatusCode.NotFound;
                    }
                    else
                    {
                        ret.code = "0";
                        ret.status = true;
                        ret.description = "Request Processed Successfully";
                        ret.HttpStatusCode = System.Net.HttpStatusCode.OK;

                        ret.Limit.dailyLimit = transactionlimit.DailyLimit;
                        ret.Limit.monthlyLimit = transactionlimit.MonthlyLimit;
                        ret.Limit.weeklyLimit = transactionlimit.WeeklyLimit;
                        ret.Limit.yearlyLimit = transactionlimit.YearlyLimit;
                        ret.Limit.transactionLimit = transactionlimit.TransactionLimit;

                    }
                }
                return ret;
           
        }
    }
}
