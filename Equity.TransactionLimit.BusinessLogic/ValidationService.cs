﻿using Equity.TransactionLimit.DTO.Models.Requests;
using Equity.TransactionLimit.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Equity.TransactionLimit.BusinessLogic
{
    public class ValidationService : IValidationService
    {
        public async Task<bool> ValidateRequest(CreateGlobalLimitRequestParameter globalLimitRequest)
        {
            return await Task.Run(() => {
                bool allfields = ValidateAllFields(globalLimitRequest);
                bool basedata = ValidateBaseData(globalLimitRequest);
                bool alllimitspositive = ValidateAllLimits(globalLimitRequest);

                return allfields && basedata && alllimitspositive;
            });
            
            
        }

        public async Task<bool> ValidateRequest(CreateTransactionLimitRequestParameter transactionLimitRequest)
        {
            return await Task.Run(() => {
                bool allfields = ValidateAllFields(transactionLimitRequest);
                bool basedata = ValidateBaseData(transactionLimitRequest);
                bool alllimitspositive = ValidateAllLimits(transactionLimitRequest);

                return allfields && basedata && alllimitspositive;
            });
        }

        public async Task<bool> ValidateRequest(GetTransactionLimitRequestParameter transactionLimitRequest)
        {
            return await Task.Run(() => {
                bool allfields = ValidateAllFields(transactionLimitRequest);
                bool basedata = ValidateBaseData(transactionLimitRequest);
               
                return allfields && basedata;
            });
        }

        /// <summary>
        /// validate whether all fields have been supplied
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool ValidateAllFields<T>(T request) where T : LimitRequestBase, new()
        {
            //In a real world solution, we'd implement some other form of input validation, but this 
            //method will validate our objects just fine given that all parameters are mandatory
            return !request.GetType()
                           .GetProperties()
                           .Where(pi => pi.PropertyType == typeof(string))
                           .Select(pi => (string)pi.GetValue(request))
                           .Any(value => string.IsNullOrEmpty(value));

        }

        /// <summary>
        /// validate all base data is valid. Here we demonstrate a simple way of validating that we are making a request for the right bank, currency, channel, etc.
        /// </summary>
        /// <param name="limitRequestBase"></param>
        /// <returns></returns>
        private bool ValidateBaseData(LimitRequestBase limitRequestBase)
        {
            bool isvalid = false;
            if (limitRequestBase.BankCode == "54"
               && (new List<string>() { "way4", "eazzyapp" }.Contains(limitRequestBase.Channel.ToLowerInvariant())
               && limitRequestBase.CountryCode == "KE"
               && limitRequestBase.Currency == "KES"
               && limitRequestBase.BankCode == "54")
               )
            {
                isvalid = true;
            }

            return isvalid;
        }

        /// <summary>
        /// Validate all limits are positive or 0 and are recognisable numbers
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool ValidateAllLimits<T>(T request) where T : LimitRequestBase, new()
        {
            //In a real world solution, we'd implement some other form of input validation, but this 
            //method will validate our objects just fine given that all parameters are mandatory
            decimal i;
            return !request.GetType()
                           .GetProperties()
                           .Where(pi => pi.PropertyType == typeof(string) && pi.Name.EndsWith("Limit"))
                           .Select(pi => (string)pi.GetValue(request))
                           .Any(value => (value.Contains("-") 
                                         || !decimal.TryParse(value, out i)));

        }
    }
}
